#
# Be sure to run `pod lib lint AdboxNetworkLib.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'AdboxNetworkLib'
  s.version          = '0.1.1'
  s.summary          = 'Adbox network utilities'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
Adbox network library and utilities for internet connection.
                       DESC

  s.homepage         = 'https://alekscbarragan@bitbucket.org/alekscbarragan/adboxnetworklib.git'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'izhuster' => 'leks.bar@icloud.com' }
  s.source           = { :git => 'https://alekscbarragan@bitbucket.org/alekscbarragan/adboxnetworklib.git', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'
  s.swift_version = '4.2'
  s.ios.deployment_target = '11.0'
  s.source_files = 'AdboxNetworkLib/Classes/**/*'
  
  # s.resource_bundles = {
    #'AdboxNetworkLib' => ['AdboxNetworkLib/Assets/*.png']
    #}

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  # s.dependency 'AFNetworking', '~> 2.3'
end
