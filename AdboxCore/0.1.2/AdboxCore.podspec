#
# Be sure to run `pod lib lint AdboxCore.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'AdboxCore'
  s.version          = '0.1.2'
  s.summary          = 'Adbox core application components.'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
Adbox core application components for reuse in company's iOS projects.
                       DESC

  s.homepage         = 'https://alekscbarragan@bitbucket.org/alekscbarragan/adboxcore.git'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'Aleks C. Barragan' => 'leks.bar@icloud.com' }
  s.source           = { :git => 'https://alekscbarragan@bitbucket.org/alekscbarragan/adboxcore.git', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'
  s.swift_version = '4.2'
  s.ios.deployment_target = '11.0'

  s.source_files = 'AdboxCore/Classes/**/*', 'AdboxCore/Views/**/*.{swift,xib,storyboard}', 'AdboxCore/Utilities/**/*.swift'
  
  s.resource_bundles = {
      'AdboxCore' => ['AdboxCore/Assets/*']
  }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  s.frameworks = 'UIKit'
  s.dependency 'AdboxNetworkLib', '~> 0.1'
  s.dependency 'AdboxUserInputValidations', '~> 0.1'
  s.dependency 'TinyConstraints'
  s.dependency 'AloeStackView'
end
